import { jsPDF } from "jspdf";
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { FileOpener } from '@capacitor-community/file-opener';
import { Capacitor } from "@capacitor/core";
import mime from "mime";

const resizeImage = function (settings) {
    let file = settings.file;
    let maxWidth = settings?.maxWidth ?? 1080;
    let maxHeigth = settings?.maxHeigth ?? 1920;
    let reader = new window.FileReader();
    let image = new window.Image();
    let canvas = document.createElement('canvas');

    let resize = function () {
        let width = image.width;
        let height = image.height;

        while (width > maxWidth || height > maxHeigth) {
            if (width > maxWidth) {
                height *= maxWidth / width;
                width = maxWidth;
            }
            if (height > maxHeigth) {
                width *= maxHeigth / height;
                height = maxHeigth;
            }
        }

        canvas.width = width;
        canvas.height = height;
        canvas.getContext('2d').drawImage(image, 0, 0, width, height);
        return canvas.toDataURL('image/jpeg');
    };

    return new Promise(function (ok, no) {
        if (!file.type.match(/image.*/)) {
            no(new Error("Not an image"));
            alert("Le fichier n'est pas pris en charge, veuillez mettre une image");
            return;
        }
        reader.onload = function (readerEvent) {
            image.onload = function () {
                return ok(resize());
            };
            image.src = readerEvent.target.result;
        };
        reader.readAsDataURL(file);
    });
};

const isImage = async function (file) {
    //Verification de l'extension
    const acceptImage = [
        // "image/bmp",
        // "image/gif",
        "image/jpeg",
        "image/png"
    ]
    if (!acceptImage.includes(file['type']))
        return false

    //Verification de la signature
    const signatures = [
        [0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A], // PNG
        [0xFF, 0xD8, 0xFF], // JPEG
    ];
    const maxSignatureLength = Math.max(
        ...signatures.map(signature => signature.length),
    );
    const arrayBuffer = await file.slice(0, maxSignatureLength).arrayBuffer();
    const uint8Array = new window.Uint8Array(arrayBuffer);

    return signatures.some(
        signature => signature.every((byte, index) => byte === uint8Array[index]),
    );
}

const isPdf = async function (file) {
    //Verification de l'extension
    if (!file['type'].includes('pdf'))
        return false


    //Verification de la signature du PDF
    const signature = [0x25, 0x50, 0x44, 0x46, 0x2D] // PDF
    const arrayBuffer = await file.slice(0, signature.length).arrayBuffer();
    const uint8Array = new window.Uint8Array(arrayBuffer);
    if (!signature.every((byte, index) => byte === uint8Array[index]))
        return false

    //Verification de la signature du HEADER
    return new Promise(function (resolve, reject) {
        let fileReader = new window.FileReader();

        fileReader.onload = function (e) {
            let data = e.target.result.substr(0, 8);
            let regexPDF = new RegExp("%PDF-1.[0-7]");
            if (data.match(regexPDF)) {
                return resolve(true)
            } else {
                return resolve(false)
            }
        };

        fileReader.readAsText(file);
    })
}

const handleFiles = async function (files) {
    let data = []

    for (let file of files) {
        let res = await handleFile(file)

        data.push(res)
    }

    return data
}

const handleFile = async function (file, defaultExtention = null) {
    let extension = defaultExtention ?? file.name.slice(file.name.lastIndexOf('.'))
    let finalFile
    if (await isImage(file)) {
        const config = {
            file: file,
            maxWidth: 1000,
            maxHeigth: 1900,
        };
        finalFile = await resizeImage(config)
    } else {
        finalFile = await inputFileToBase64(file);
    }
    finalFile = finalFile.split(',')[1]

    let data = {
        "file": finalFile,
        "extension": extension,
        "type": file['type']
    }

    return data
}

const imageToPDF = function (imageDataURL) {
    const pdf = new jsPDF({
        orientation: "portrait",
        unit: "px"
    });

    pdf.addImage(imageDataURL, 0, 0);

    return new window.File([pdf.output("blob")], "myDoc.pdf", {
        type: "application/pdf",
    })
}

const inputFileToBase64 = file => new Promise((resolve, reject) => {
    const reader = new window.FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});

const base64ToBlob = function (base64, type = 'application/pdf') {
    let data
    if(base64.split(',')[1]){
        type = base64.split(',')[0].split(':')[1].split(';')[0]
        data = base64.split(',')[1]
    }
    else{
        data = base64
    }
    
    // Convert base64 to blob
    let byteString = atob(data);
    let arrayBuffer = new window.ArrayBuffer(byteString.length);
    let int8Array = new window.Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
        int8Array[i] = byteString.charCodeAt(i);
    }
    let blob = new window.Blob([int8Array], { type: type });
    return blob;
}

const localFileToBlob = (filePath) => {
    return fetch(filePath)
        .then((r) => r.blob())
        .then((blobFile) => {
            return blobFile;
        });
}

function escapeFilename(filename) {
    // normalize accented characters to their basic latin equivalents
    return filename.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[^a-zA-Z0-9_\-\.]/g, '_');
}

const downloadFile = function (data, title = "document") {
    return downloadBlob(data, title);
}

const downloadBlob = function (data, title = "document") {
    // Échapper les caractères spéciaux dans le nom de fichier
    let escapedFilename = escapeFilename(title);

    //if don't have extension, add it
    if (!escapedFilename.includes('.')) {
        let extension = mime.getExtension(data.type);
        escapedFilename = escapedFilename + '.' + extension
    }

    // Download a file thanks to url and title passed in parameter with capacitor if exist
    if (Capacitor.isNativePlatform()) {
        capacitorDownload(escapedFilename, data, data.type);
    }
    else {
        let url = URL.createObjectURL(data);
    
        let a = document.createElement('a');
        a.href = url;
        a.download = escapedFilename;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }
}

function capacitorDownload(filename, data, mimeType) {
    const reader = new FileReader();
    reader.readAsDataURL(data);
    reader.onloadend = async () => {
        try {
            const base64data = reader.result.split(',')[1];
            let extension = mime.getExtension(mimeType);

            // Write the file to the filesystem
            const fileName = filename + '.' + extension;
            await Filesystem.writeFile({
                path: fileName,
                data: base64data,
                directory: Directory.Documents,
                encoding: Encoding.BASE64
            });
 
            // Get the URI of the written file
            const fileUri = await Filesystem.getUri({
                path: fileName,
                directory: Directory.Documents
            });
 
            const fileOpenerOptions = {
                filePath: fileUri.uri,
                contentType: mimeType,
                openWithDefault: true,
            };
 
            await FileOpener.open(fileOpenerOptions);
        } catch (error) {
            console.error('Error downloading file:', error);
        }
    };
}

const openFileBase64 = function (base64, type = 'application/pdf') {
    // Open a file thanks to base64 and type passed in parameter
    let blob = base64ToBlob(base64, type);
    openFileBlob(blob);
}

const openFileBlob = async function (blob) {
    // Open a file thanks to blob passed in parameter with cordova if exist
    let url = URL.createObjectURL(blob);
    if (Capacitor.isNativePlatform()) {
        capacitorDownload("Fichier", blob, blob.type);
    } else {
        window.open(url, '_blank');
    }
}

let fileUtils = {
    inputFileToBase64,
    resizeImage,
    handleFiles,
    handleFile,
    imageToPDF,
    isImage,
    isPdf,
    downloadFile,
    downloadBlob,
    localFileToBlob,
    openFileBase64,
    openFileBlob,
    base64ToBlob
}

export default fileUtils;

export {
    inputFileToBase64,
    resizeImage,
    handleFiles,
    handleFile,
    imageToPDF,
    isImage,
    isPdf,
    downloadFile,
    downloadBlob,
    localFileToBlob,
    openFileBase64,
    openFileBlob,
    base64ToBlob
}
